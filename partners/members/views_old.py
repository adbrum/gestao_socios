from django.http import Http404
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.shortcuts import resolve_url as r
from partners.subscriptions.forms import SubscriptionForm, MemberContactForm, MemberAddressForm

from partners.core.models import Member, MemberContact, MemberAddress


def new(request):
    """Dispacher - Ponto de Entrada"""
    if request.method == 'POST':
        return create(request)

    return empty_form(request)


def empty_form(request):
    """Se o pedido vier como GET - mostra o formulário vazio"""
    return render(request, 'members/subscription_form.html',
                  {'form': SubscriptionForm(),
                   'form_contact': MemberContactForm(),
                   'form_address': MemberAddressForm(),
                   #  'form_situation': SituationForm(),
                   #  'form_occupation': OccupationForm()
                   })


def create(request):
    """Se o pedido vier como POST"""
    form = SubscriptionForm(request.POST)
    form_contact = MemberContactForm(request.POST)
    form_address = MemberAddressForm(request.POST)
    # form_situation = SituationForm(request.POST)
    # form_occupation = OccupationForm(request.POST)
    #
    print('OOOOOOOOOOOOOO: ', form_contact)

    if not form.is_valid():
        print('NÂO VALIDO')
        """Caso de insucesso - aborta o pedido"""
        return render(request, 'members/subscription_form.html',
                      {'form': form, 'form_contact': form_contact,
                       'form_address': form_address
                       # 'form_situation': form_situation,
                       # 'form_occupation': form_occupation
                       })

    contact = MemberContact.objects.create(**form_contact.cleaned_data)
    address = MemberAddress.objects.create(**form_address.cleaned_data)
    # situation = Situation.objects.create(**form_situation.cleaned_data)
    # occupation = Occupation.objects.create(**form_occupation.cleaned_data)

    # subscription = form.save()  # Quando é utilizado o forms.ModelForm
    subscription = Member.objects.create(member_contact_id=contact.pk,
                                         member_address_id=address.pk,
                                         # situation_id=situation.pk,
                                         # occupation_id=occupation.pk,
                                         name=form.cleaned_data['name'],
                                         sex=form.cleaned_data['sex'],
                                         birthdate=form.cleaned_data['birthdate'],
                                         crm_number=form.cleaned_data['crm_number'],
                                         nationality=form.cleaned_data['nationality'],
                                         qualifications=form.cleaned_data['qualifications'],
                                         member_number=form.cleaned_data['member_number'],
                                         profession=form.cleaned_data['profession'],
                                         company=form.cleaned_data['company'],
                                         parent_level=form.cleaned_data['parent_level'],
                                         renumbering_date=form.cleaned_data['renumbering_date'],
                                         member_number_before_renumbering=form.cleaned_data[
                                             'member_number_before_renumbering'],
                                         lock_renumbering=form.cleaned_data['lock_renumbering'],
                                         member_category=form.cleaned_data['member_category']
                                         )  # Quando é utilizado o forms.Form no form.

    """Caso de sucesso!"""

    # # Success feedback
    # messages.success(request, 'Inscrição realizada com sucesso!')
    print('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX: ', subscription.pk)

    return HttpResponseRedirect(r('memberprofiles:detail', subscription.pk))


def detail(request, pk):
    print('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP', pk)
    try:
        subscription = Member.objects.get(pk=pk)
    except Member.DoesNotExist:  # Levanta o erro se não existir na base de dados.
        raise Http404

    return render(request, 'members/subscription_detail.html', {'subscription': subscription})
