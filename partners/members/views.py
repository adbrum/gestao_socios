from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.shortcuts import resolve_url as r

from partners.core.models import Members
from partners.members.forms import MemberProfileForm, EditMemberProfileForm


def listsubscription(request):
    print('LISTA')
    data = Members.objects.all()
    if not data:
        context = {
            'list': data,
            'tamLista': 0,
        }

        return render(request, "members/index.html", context)
    else:
        context = {
            'list': data,
            'tamLista': 1,
        }

        return render(request, "members/index.html", context)


def new(request):
    print('NEW')
    if request.method == 'POST':
        return create(request)

    return empty_form(request)


def empty_form(request):
    print('VAZIO')
    return render(request, 'members/add.html',
                  {'form': MemberProfileForm()})


# def empty_prototipo_form(request):
#     return render(request, 'members/subscription_form.html',
#                   {'form': MemberProfileForm()})


def create(request):
    form = MemberProfileForm(request.POST or None)
    if not (form.is_valid()):
        return render(request, 'members/add.html', locals())
    else:
        Members.objects.create(**form.cleaned_data)

        return HttpResponseRedirect(r('memberprofiles:success'))


def editMemberProfile(request, pk):
    perfil = get_object_or_404(Members, pk=pk)

    argUser = perfil.name
    argEmail = perfil.email

    form = EditMemberProfileForm(request.POST or None, instance=perfil)

    if not (form.is_valid()):
        return render(request, 'members/edit.html', locals())
    else:
        form.save()
        return HttpResponseRedirect(r('memberprofiles:success'))


def success(request):
    return render(request, 'members/subscription_success.html')


def delDataModalMemberProfile(request):
    id_list = []
    if request.is_ajax():
        select = request.POST.getlist('valores[]')

        for pk in select:
            id_list.append(int(pk))

    users = Members.objects.filter(pk__in=id_list, situation=True)

    return render(request, 'members/desativar_modal.html', {'users': users})


def activeDataModalMemberProfile(request):
    print('ATIVAR')
    id_list = []
    if request.is_ajax():
        select = request.POST.getlist('valores[]')

        for pk in select:
            id_list.append(int(pk))

    user = Members.objects.filter(pk__in=id_list, situation=False)
    # for i in user:
    #     i.user.us
    return render(request, 'members/ativar_modal.html', {'users': user})


def delConfirmeMemberProfile(request, *args, **kwargs):
    select = request.POST.getlist('valores_list[]')
    for valor in select:
        Members.objects.filter(pk=valor).update(situation=False)

    return HttpResponseRedirect(r('memberprofiles:list'))


def activeConfirmeMemberProfile(request, *args, **kwargs):
    select = request.POST.getlist('valores_list[]')
    for valor in select:
        Members.objects.filter(pk=valor).update(situation=True)

    return HttpResponseRedirect(r('memberprofiles:list'))


def fichaMemberProfile(request, *args, **kwargs):
    idMemberProfile = kwargs['idMemberProfile']

    editar = True
    saveNew = False

    users = Members.objects.get(id=idMemberProfile)
    argCode = users.pk

    if request.method == 'GET':
        url = reverse('/socio/ficha/' + idMemberProfile)
        return HttpResponseRedirect(url)
    else:

        return HttpResponseRedirect(r('memberprofiles:list'))
