from django import forms

from partners.core.models import Member, MemberContact, MemberAddress, MemberDocuments, Situation, Occupation


class SubscriptionForm(forms.ModelForm):
    class Meta:
        model = Member
        # fields = '__all__'
        exclude = ['member_contact_id', ]

    def clean_name(self):  # Este método é o clean do campo name
        name = self.cleaned_data['name']
        words = [w.capitalize() for w in name.split()]
        return ' '.join(words)


class MemberContactForm(forms.ModelForm):
    class Meta:
        model = MemberContact
        fields = '__all__'


class MemberAddressForm(forms.ModelForm):
    class Meta:
        model = MemberAddress
        fields = '__all__'


class MemberDocumentsForm(forms.ModelForm):
    class Meta:
        model = MemberDocuments
        fields = '__all__'


class SituationForm(forms.ModelForm):
    class Meta:
        model = Situation
        fields = '__all__'


class OccupationForm(forms.ModelForm):
    class Meta:
        model = Occupation
        fields = '__all__'
