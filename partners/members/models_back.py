from django.db import models


class MemberProfile(models.Model):
    MALE = 0
    FEMALE = 1

    SEX = (
        (MALE, 'Masculino'),
        (FEMALE, 'Feminino'),
    )
    member_contact = models.ForeignKey('MemberContact', verbose_name='contato')
    member_address = models.ForeignKey('MemberAddress', verbose_name='endereço')
    situation = models.ForeignKey('Situation', verbose_name='situação')
    occupation = models.ForeignKey('Occupation', verbose_name='ocupação')
    # anchor_member_id = models.ForeignKey()
    # dun_id = models.IntegerField()
    # marital_status_id = models.IntegerField()
    name = models.CharField('nome', max_length=128)
    sex = models.BooleanField('sexo', choices=SEX)
    birthdate = models.DateField('data nascimento', blank=True)
    crm_number = models.CharField('número CRM', max_length=64)
    nationality = models.CharField('nacionalidade', max_length=64, blank=True)
    qualifications = models.CharField('qualificação', max_length=64, blank=True)
    member_number = models.IntegerField('número sócio')
    profession = models.CharField('profissão', max_length=64, blank=True)
    company = models.CharField('compania', max_length=64, blank=True)
    parent_level = models.IntegerField('nível parental', blank=True)
    renumbering_date = models.DateField(blank=True)
    member_number_before_renumbering = models.IntegerField(blank=True)
    lock_renumbering = models.BooleanField(blank=True)
    member_category = models.IntegerField('sócio categoria', blank=True)

    class Meta:
        ordering = ["name"]
        verbose_name = 'membro'
        verbose_name_plural = 'membros'

    def __str__(self):
        return self.name


class MemberContact(models.Model):
    # member_id = models.IntegerField('Sócio número', blank=True)
    email = models.EmailField('email', max_length=128, blank=True)
    telephone_1 = models.CharField('telemóvel', max_length=32, blank=True)
    telephone_2 = models.CharField('telefone', max_length=32, blank=True)

    class Meta:
        verbose_name = 'membro contato'
        verbose_name_plural = 'membro contatos'

    def __str__(self):
        return self.email


class MemberAddress(models.Model):
    # member_id = models.IntegerField('Sócio número', blank=True)
    # country_id = models.ForeignKey()
    # zone_id = models.ForeignKey()
    city = models.CharField('cicade', max_length=128, blank=True)
    residence_address = models.CharField('morada', max_length=128, blank=True)
    biling_address = models.CharField(max_length=128, blank=True)
    postcode = models.CharField('código postal', max_length=32, blank=True)

    class Meta:
        verbose_name = 'Endereço'
        verbose_name_plural = 'Endereços'

    def __str__(self):
        return self.city


class MemberDocuments(models.Model):
    # member_id = models.IntegerField('Sócio número', blank=True)
    document_id = models.ForeignKey('Documents', verbose_name='documento', blank=True)
    document_value = models.CharField(max_length=255, blank=True)

    class Meta:
        verbose_name = 'documento'
        verbose_name_plural = 'documentos sócio'

    def __str__(self):
        return self.document_id


class Situation(models.Model):
    description = models.TextField('descrição', max_length=128, blank=True)
    active = models.BooleanField('ativo', blank=True)
    free_quotas = models.BooleanField('livre de quotas', blank=True)
    free_subscription = models.BooleanField('livre de subscrição', blank=True)
    inhibited_quotas = models.BooleanField('inibido de quotas', blank=True)
    inhibited_subscription = models.BooleanField('inibido de subscrição', blank=True)
    inhibited_ticket = models.BooleanField('inibido de ticket', blank=True)
    inhibited_season_ticket = models.BooleanField('inibido de temporada', blank=True)

    class Meta:
        verbose_name = 'situação'
        verbose_name_plural = 'situações'


class Occupation(models.Model):
    description = models.TextField('descrição', max_length=128, blank=True)

    class Meta:
        verbose_name = 'ocupação'
        verbose_name_plural = 'ocupações'


class Documents(models.Model):
    description = models.TextField('descrição', max_length=128, blank=True)

    class Meta:
        verbose_name = 'documento'
        verbose_name_plural = 'documentos'
