# from django.db import models
# from django.db.models import Max
#
#
# def n_member():
#     """
#     Pega o maior número de sócio e adiciona + 1.
#     :return Nº de sócio + 1:
#     """
#     value = MemberProfile.objects.all().aggregate(Max('member_number'))
#     number = value.get('member_number__max')
#     if number is None:
#         number = value['member_number__max'] = 1
#     else:
#         number = int(value.get('member_number__max')) + 1
#
#     return number
#
#
# class MemberProfile(models.Model):
#     # Dados sócio
#     member_number = models.IntegerField('número sócio', default=n_member)
#     admission_date = models.DateField('data admissão', blank=True, null=True)
#     document_type = models.CharField('tipo de documento', max_length=64, blank=True, default=1)
#     doc_number = models.CharField('número', max_length=64, blank=True)
#     doc_emmission = models.CharField('local de emissão', max_length=64, blank=True)
#     doc_date_emission = models.DateField('data de emissão', blank=True, null=True)
#     doc_date_validity = models.DateField('data de validade', blank=True, null=True)
#     situation = models.CharField('situação', max_length=64, blank=True)
#     last_quota = models.DateField('última Quota Paga', blank=True, null=True)
#     category = models.CharField('categoria', max_length=64, blank=True)
#     profile = models.CharField('perfil', max_length=64, blank=True)
#
#     # Dados pessoais
#     name = models.CharField('nome', max_length=128)
#     sex = models.CharField('sexo', max_length=64, blank=True)
#     birthdate = models.DateField('data nascimento', blank=True, null=True)
#     nationality = models.CharField('nacionalidade', max_length=64, blank=True)
#     naturalness = models.CharField('naturalidade', max_length=64, blank=True)
#     marital_status = models.CharField('estado civil', max_length=64, blank=True)
#     qualifications = models.CharField('habilitações', max_length=64, blank=True)
#     profession = models.CharField('profissão', max_length=64, blank=True)
#     professional_situation = models.CharField('situação profissional', max_length=64, blank=True)
#     father = models.CharField('pai', max_length=64, blank=True)
#     mother = models.CharField('mãe', max_length=64, blank=True)
#     company = models.CharField('compania', max_length=64, blank=True)
#     # parent_level = models.IntegerField('nível parental', blank=True, null=True)
#     # renumbering_date = models.DateField(blank=True, null=True)
#     # member_number_before_renumbering = models.IntegerField(blank=True, null=True)
#     # lock_renumbering = models.BooleanField(blank=True)
#
#     # morada
#     residence_address = models.CharField('morada', max_length=128, blank=True)
#     city = models.CharField('cicade', max_length=128, blank=True)
#     biling_address = models.CharField(max_length=128, blank=True)
#     postcode = models.CharField('código postal', max_length=64, blank=True)
#     country = models.CharField('país', max_length=64, blank=True)
#
#     # contatos
#     email = models.EmailField('email', max_length=128, blank=True)
#     telephone_1 = models.CharField('telemóvel', max_length=64, blank=True)
#     telephone_2 = models.CharField('telefone', max_length=64, blank=True)
#
#     #######
#
#     NIF = models.CharField('Nº de Identificação Fiscal', max_length=64, blank=True)
#
#     SWIFT = models.CharField('SWIFT', max_length=64, blank=True)
#
#     tipo_cobranca = models.CharField('Tipo de Cobrança', max_length=64, blank=True)
#
#     cobrador = models.CharField('Cobrador', max_length=64, blank=True)
#
#     prioridade = models.CharField('Periodicidade', max_length=64, blank=True)
#
#     banco = models.CharField('Banco', max_length=64, blank=True)
#
#     pais_banco = models.CharField('País do Banco', max_length=64, blank=True)
#
#     NIB_IBAN = models.CharField('NIB/IBAN', max_length=64, blank=True)
#
#
#
#     class Meta:
#         ordering = ["member_number"]
#         verbose_name = 'sócio'
#         verbose_name_plural = 'sócios'
#
#     def __str__(self):
#         return self.name
#
# # class Category(models.Model):
# #     BOOL_CHOICES = ((True, 'Sim'), (False, 'Não'))
# #
# #     description = models.TextField('descrição', max_length=128, blank=True)
# #     quota = models.BooleanField('sexo', choices=BOOL_CHOICES, blank=True)
# #     subscription = models.BooleanField('sexo', choices=BOOL_CHOICES, blank=True)
# #     ticket = models.BooleanField('sexo', choices=BOOL_CHOICES, blank=True)
# #     ticket_season = models.BooleanField('sexo', choices=BOOL_CHOICES, blank=True)
# #
# #     def __str__(self):
# #         return self.description
#
# # class MemberContact(models.Model):
# #     # member_id = models.IntegerField('Sócio número', blank=True)
# #     email = models.EmailField('email', max_length=128, blank=True)
# #     telephone_1 = models.CharField('telemóvel', max_length=64, blank=True)
# #     telephone_2 = models.CharField('telefone', max_length=64, blank=True)
# #
# #     class Meta:
# #         verbose_name = 'membro contato'
# #         verbose_name_plural = 'membro contatos'
# #
# #     def __str__(self):
# #         return self.email
# #
# #
# # class MemberAddress(models.Model):
# #     # member_id = models.IntegerField('Sócio número', blank=True)
# #     # country_id = models.ForeignKey()
# #     # zone_id = models.ForeignKey()
# #     city = models.CharField('cicade', max_length=128, blank=True)
# #     residence_address = models.CharField('morada', max_length=128, blank=True)
# #     biling_address = models.CharField(max_length=128, blank=True)
# #     postcode = models.CharField('código postal', max_length=64, blank=True)
# #
# #     class Meta:
# #         verbose_name = 'Endereço'
# #         verbose_name_plural = 'Endereços'
# #
# #     def __str__(self):
# #         return self.city
# #
# #
# # class MemberDocuments(models.Model):
# #     # member_id = models.IntegerField('Sócio número', blank=True)
# #     document_id = models.ForeignKey('Documents', verbose_name='documento', blank=True)
# #     document_value = models.CharField(max_length=255, blank=True)
# #
# #     class Meta:
# #         verbose_name = 'documento'
# #         verbose_name_plural = 'documentos sócio'
# #
# #     def __str__(self):
# #         return self.document_id
# #
# #
# # class Situation(models.Model):
# #     description = models.TextField('descrição', max_length=128, blank=True)
# #     active = models.BooleanField('ativo', blank=True)
# #     free_quotas = models.BooleanField('livre de quotas', blank=True)
# #     free_subscription = models.BooleanField('livre de subscrição', blank=True)
# #     inhibited_quotas = models.BooleanField('inibido de quotas', blank=True)
# #     inhibited_subscription = models.BooleanField('inibido de subscrição', blank=True)
# #     inhibited_ticket = models.BooleanField('inibido de ticket', blank=True)
# #     inhibited_season_ticket = models.BooleanField('inibido de temporada', blank=True)
# #
# #     class Meta:
# #         verbose_name = 'situação'
# #         verbose_name_plural = 'situações'
# #
# #
# # class Occupation(models.Model):
# #     description = models.TextField('descrição', max_length=128, blank=True)
# #
# #     class Meta:
# #         verbose_name = 'ocupação'
# #         verbose_name_plural = 'ocupações'
# #
# #
# # class Documents(models.Model):
# #     description = models.TextField('descrição', max_length=128, blank=True)
# #
# #     class Meta:
# #         verbose_name = 'documento'
# #         verbose_name_plural = 'documentos'
