from django.conf.urls import url, include

from partners.members.views import new, editMemberProfile, listsubscription, \
    delDataModalMemberProfile, activeDataModalMemberProfile, delConfirmeMemberProfile, activeConfirmeMemberProfile, \
    success

urlpatterns = [
    url(r'^$', new, name='new'),
    url(r'^edit/(?P<pk>\d+)$', editMemberProfile, name="edit"),
    url(r'^lista/', listsubscription, name='list'),
    url(r'^deletar/', delDataModalMemberProfile, name='delete'),
    url(r'^ativar/', activeDataModalMemberProfile, name='activate'),
    url(r'^confirmedeletar/', delConfirmeMemberProfile, name='delconfirm'),
    url(r'^confirmeativar/', activeConfirmeMemberProfile, name='activeconfirm'),
    url(r'^sucesso/', success, name='success'),
    # url(r'^dadospessoais/', include('partners.subscriptions.urls',
    #                                 namespace='subscription')),
]
