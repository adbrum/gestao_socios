from django.contrib import admin

from partners.core.models import Colecting_types, MemberContact, Documents
from partners.core.models import Colector_groups
from partners.core.models import Colectors
from partners.core.models import Countries, Member_documents, Profession_members, Profession_situations
from partners.core.models import Countries
from partners.core.models import Country_zones
from partners.core.models import Marital_statuses
from partners.core.models import Member_categories
from partners.core.models import Members, Situation, Ocupations, Qualifications
from partners.core.models import Nationalities
from partners.core.models import Parent_levels

admin.site.register(Members)
admin.site.register(Situation)
admin.site.register(Ocupations)
admin.site.register(Qualifications)
admin.site.register(Marital_statuses)
admin.site.register(Nationalities)
admin.site.register(Countries)
admin.site.register(Country_zones)
admin.site.register(Member_categories)
admin.site.register(Parent_levels)
admin.site.register(Colectors)
admin.site.register(Colector_groups)
admin.site.register(Colecting_types)
admin.site.register(MemberContact)
admin.site.register(Member_documents)
admin.site.register(Profession_members)
admin.site.register(Profession_situations)
admin.site.register(Documents)

