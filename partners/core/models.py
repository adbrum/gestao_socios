from django.db import models
from django.db.models import Max

def n_member():
    """
    Pega o maior número de sócio e adiciona + 1.
    :return Nº de sócio + 1:
    """
    value = Members.objects.all().aggregate(Max('member_number'))
    number = value.get('member_number__max')
    if number is None:
        number = value['member_number__max'] = 1
    else:
        number = int(value.get('member_number__max')) + 1

    return number


class Members(models.Model):
    situation_id = models.ForeignKey('Situation', verbose_name='situação')
    occupation_id = models.ForeignKey('Ocupations', verbose_name='ocupação')
    qualification_id = models.ForeignKey('Qualifications', verbose_name='qualificação')
    marital_status_id = models.ForeignKey('Marital_statuses', verbose_name='estado cívil')
    nationality_id = models.ForeignKey('Nationalities', verbose_name='nacionalidade')
    member_category_id = models.ForeignKey('Member_categories', verbose_name='categorias de membros')
    parent_level_id = models.ForeignKey('Parent_levels', verbose_name='nível parental')
    collector_id = models.ForeignKey('Colectors', verbose_name='coletores')
    # anchor_member_id = models.ForeignKey('Anchor_member', verbose_name='coletores')
    first_name = models.CharField('Nome', max_length=64)
    last_name = models.CharField('Apelido', max_length=64)
    member_number = models.IntegerField('nº do sócio', default=n_member)
    sex = models.BooleanField('sexo', default=True)
    birthdate = models.DateField('data nascimento', blank=True)
    crm_number = models.CharField('compania', max_length=64, blank=True)
    lock_renumbering = models.BooleanField('lock_renumbering', default=True)

    class Meta:
        ordering = ["member_number"]
        verbose_name = 'membro'
        verbose_name_plural = 'membros'

    def __str__(self):
        return self.name


class Situation(models.Model):
    description = models.TextField('descrição', max_length=128, blank=True)
    active = models.BooleanField('ativo', blank=True)
    free_clubfree = models.BooleanField('livre de quotas', blank=True)
    free_subscription = models.BooleanField('livre de subscrição', blank=True)
    inhibited_clubfree = models.BooleanField('inibido de quotas', blank=True)
    inhibited_subscription = models.BooleanField('inibido de subscrição', blank=True)
    inhibited_ticket = models.BooleanField('inibido de ticket', blank=True)
    inhibited_season_ticket = models.BooleanField('inibido de temporada', blank=True)

    class Meta:
        verbose_name = 'situação'
        verbose_name_plural = 'situações'


class Ocupations(models.Model):
    description = models.TextField('descrição', max_length=128, blank=True)

    class Meta:
        verbose_name = 'ocupação'
        verbose_name_plural = 'ocupações'

    def __str__(self):
        return self.description


class Qualifications(models.Model):
    description = models.TextField('descrição', max_length=128, blank=True)

    class Meta:
        verbose_name = 'qualificação'
        verbose_name_plural = 'qualificações'

    def __str__(self):
        return self.description


class Marital_statuses(models.Model):
    description = models.TextField('descrição', max_length=128, blank=True)

    class Meta:
        verbose_name = 'estado cívil'
        # verbose_name_plural = 'qualificações'

    def __str__(self):
        return self.description


class Nationalities(models.Model):
    country_id = models.ForeignKey('Countries', verbose_name='paises')
    first_denomination = models.TextField('1ª denominação', max_length=64, blank=True)
    second_denomination = models.TextField('2ª denominação', max_length=64, blank=True)

    class Meta:
        verbose_name = 'nacionalidades'
        # verbose_name_plural = 'qualificações'

    def __str__(self):
        return self.first_denomination


class Countries(models.Model):
    description = models.TextField('descrição', max_length=128, blank=True)

    class Meta:
        verbose_name = 'país'
        verbose_name_plural = 'paises'

    def __str__(self):
        return self.description


class Country_zones(models.Model):
    description = models.TextField('descrição', max_length=128, blank=True)

    class Meta:
        verbose_name = 'zona do país'
        verbose_name_plural = 'zonas do país'

    def __str__(self):
        return self.description


class Member_categories(models.Model):
    description = models.TextField('descrição', max_length=128, blank=True)
    # relation_profile_id = models.ForeignKey('Relations_profiles', verbose_name='nacionalidade')
    next_category_id = models.IntegerField('próxima categoria', blank=True)
    free_clubfee = models.BooleanField(default=True)
    free_special_clubfee = models.BooleanField(default=True)
    right_to_vote = models.BooleanField(default=True)
    number_votes = models.IntegerField(blank=True)
    relation_years = models.IntegerField(blank=True)
    automatic_approval = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'categoria de membro'
        verbose_name_plural = 'categorias de membros'

    def __str__(self):
        return self.description


class Parent_levels(models.Model):
    description = models.TextField('Nível parental', max_length=128, blank=True)

    # class Meta:
    #     verbose_name = 'país'
    #     verbose_name_plural = 'paises'

    def __str__(self):
        return self.description


class Colectors(models.Model):
    colector_group_id = models.ForeignKey('Colector_groups')
    description = models.TextField('descrição', max_length=128, blank=True)

    class Meta:
        verbose_name = 'cobrador'
        verbose_name_plural = 'cobradores'

    def __str__(self):
        return self.description


class Colector_groups(models.Model):
    collecting_type_id = models.ForeignKey('Colecting_types')
    description = models.TextField('descrição', max_length=128, blank=True)

    class Meta:
        verbose_name = 'grupo cobrador'
        verbose_name_plural = 'grupos cobradores'

    def __str__(self):
        return self.description


class Colecting_types(models.Model):
    description = models.TextField('descrição', max_length=128, blank=True)

    class Meta:
        verbose_name = 'tipo de cobrança'
        verbose_name_plural = 'tipos de cobrança'

    def __str__(self):
        return self.description


class MemberContact(models.Model):
    member_id = models.ForeignKey('Members', verbose_name='nº de membro')
    email = models.EmailField('email', max_length=128, blank=True)
    telephone_1 = models.CharField('telemóvel', max_length=32, blank=True)
    telephone_2 = models.CharField('telefone', max_length=32, blank=True)

    class Meta:
        verbose_name = 'membro contato'
        verbose_name_plural = 'membro contatos'

    def __str__(self):
        return self.email


class MemberAddress(models.Model):
    member_id = models.ForeignKey('Members', verbose_name='nº de membro')
    # address_type_id = models.ForeignKey()
    country_id = models.ForeignKey('Countries')
    zone_id = models.ForeignKey('Country_zones')
    city = models.CharField('cicade', max_length=128, blank=True)
    address = models.CharField('morada', max_length=128, blank=True)
    postcode = models.CharField('código postal', max_length=32, blank=True)

    class Meta:
        verbose_name = 'endereço'
        verbose_name_plural = 'endereços'

    def __str__(self):
        return self.address


class Member_documents(models.Model):
    member_id = models.ForeignKey('Members', verbose_name='nº de membro')
    document_id = models.ForeignKey('Documents', verbose_name='documento', blank=True)
    document_value = models.CharField(max_length=255, blank=True)

    class Meta:
        verbose_name = 'documento'
        verbose_name_plural = 'documentos sócio'

    def __str__(self):
        return self.document_id


class Profession_members(models.Model):
    member_id = models.ForeignKey('Members', verbose_name='nº de membro')
    description = models.TextField('descrição', max_length=128, blank=True)
    profession_situation_id = models.ForeignKey('Profession_situations', verbose_name='situação profissional')
    company = models.CharField('companhia', max_length=68, blank=True)

    class Meta:
        verbose_name = 'situação profissional'
        # verbose_name_plural = 'ocupações'

    def __str__(self):
        return self.description


class Profession_situations(models.Model):
    description = models.TextField('descrição', max_length=128, blank=True)

    class Meta:
        verbose_name = 'situação profissional'
        # verbose_name_plural = 'tipos de cobrança'

    def __str__(self):
        return self.description


class Documents(models.Model):
    description = models.TextField('descrição', max_length=128, blank=True)

    class Meta:
        verbose_name = 'documento'
        verbose_name_plural = 'documentos'
