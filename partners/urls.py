from django.conf.urls import url, include
from django.contrib import admin

from partners.core.views import home
from material.frontend import urls as frontend_urls

urlpatterns = [
    url(r'', include(frontend_urls)),
    url(r'^$', home, name='home'),
    url(r'^socio/', include('partners.members.urls',
                            namespace='memberprofiles')),
    # url(r'^inscricao/', include('partners.subscriptions.urls',
    #                             namespace='subscriptions')),

    url(r'^admin/', admin.site.urls),
]
