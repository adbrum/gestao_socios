from django.conf.urls import url


from partners.subscriptions.views import new, success, new_quick

urlpatterns = [
    url(r'^$', new, name='new'),
    url(r'^inscricaorapida/', new_quick, name='quickregister'),
    # url(r'^edit/(?P<pk>\d+)$', editSubscription, name="edit"),
    # # url(r'^lista/', listsubscription, name='list'),
    # url(r'^deletar/', delDataModalSubscription, name='delete'),
    # url(r'^ativar/', activeDataModalSubscription, name='activate'),
    # url(r'^confirmedeletar/', delConfirmeSubscription, name='delconfirm'),
    # url(r'^confirmeativar/', activeConfirmeSubscription, name='activeconfirm'),
    url(r'^sucesso/', success, name='success'),
]
