from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.shortcuts import resolve_url as r

from partners.members.models import MemberProfile
from partners.subscriptions.forms import SubscriptionForm


def new(request):
    if request.method == 'POST':
        print('NEW POST')
        return create(request)

    return empty_form(request)


def empty_form(request):
    return render(request, 'subscriptions/add.html',
                  {'form': SubscriptionForm()})


def create(request):
    form = SubscriptionForm(request.POST or None)
    print('FORM: ', request.POST)
    if not (form.is_valid()):
        return render(request, 'subscriptions/add_return.html', locals())
    else:
        MemberProfile.objects.create(**form.cleaned_data)

        return HttpResponseRedirect(r('subscriptions:success'))


def new_quick(request):
    if request.method == 'POST':
        print('NEW POST Rapido')
        return create_quick(request)

    return empty_form_quick(request)


def empty_form_quick(request):
    return render(request, 'subscriptions/add_quick_registration.html',
                  {'form': SubscriptionForm()})


def create_quick(request):
    form = SubscriptionForm(request.POST or None)
    if not (form.is_valid()):
        return render(request, 'subscriptions/add_quick_registration_return.html', locals())
    else:
        member = MemberProfile(name=form.cleaned_data['name'],
                               document_type=form.cleaned_data['document_type'],
                               doc_number=form.cleaned_data['doc_number'],
                               sex=form.cleaned_data['sex'],
                               birthdate=form.cleaned_data['birthdate'],
                               category=form.cleaned_data['category'],
                               email=form.cleaned_data['email'],
                               telephone_1=form.cleaned_data['telephone_1'],
                               telephone_2=form.cleaned_data['telephone_2'])
        member.save()

        return HttpResponseRedirect(r('subscriptions:success'))


def success(request):
    return render(request, 'subscriptions/subscription_success.html')

