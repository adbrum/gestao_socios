from django import forms
from django.forms import ModelForm

from partners.members.models import MemberProfile, n_member

BOOL_CHOICES = ((True, 'Sim'), (False, 'Não'))

default = ''
CC = 'Cartão do Cidadão'
BI = 'Bilhete de Identidade'
NIF = 'Número de Identidade Fiscal'
DOC = (
    (default, '---'),
    (CC, 'Cartão do Cidadão'),
    (BI, 'Bilhete de Identidade'),
    (NIF, 'Número de Identidade Fiscal')
)

default = ''
A = 'Ativo'
DFP = 'Demitido falta pagamento'
F = 'Falecimento'
S = 'Suspenso'
SITUACAO = (
    (default, '---'),
    (A, 'Ativo'),
    (DFP, 'Demitido falta pagamento'),
    (F, 'Falecimento'),
    (S, 'Suspenso')
)

default = ''
MALE = 'Masculino'
FEMALE = 'Feminino'
SEX = (
    ('', '---'),
    (MALE, 'Masculino'),
    (FEMALE, 'Feminino'),
)

default = ''
BA = 'Bancada'
EF = 'Efetivo'
IN = 'infantil'
SEN = 'Sénior'
CATEGORY = (
    (default, '---'),
    (BA, 'Bancada'),
    (EF, 'Efetivo'),
    (IN, 'infantil'),
    (SEN, 'Sénior'),
)

default = ''
SO = 'Sócio'
AD = 'Adpto'
CLI = 'Cliente'
FUN = 'Funcionário'
AT = 'Atleta'
SE = 'Seccionista'
PERFIL = (
    (default, '---'),
    (SO, 'Sócio'),
    (AD, 'Adpto'),
    (CLI, 'Cliente'),
    (FUN, 'Funcionário'),
    (AT, 'Atleta'),
    (SE, 'Seccionista')
)

default = ''
PT = 'Portuguesa'
BR = 'Brasileira'
MZ = 'Moçanbicana'
OUT = 'Outras Nacionalidades'

NACIONALIDADE = (
    (default, '---'),
    (PT, 'Portuguesa'),
    (BR, 'Brasileira'),
    (MZ, 'Moçanbicana'),
    (OUT, 'Outras Nacionalidades'),
)

default = ''
LiS = 'Lisboa'
POR = 'Porto'
OUT = 'Outras Naturalidades'

NATURALIDADE = (
    (default, '---'),
    (LiS, 'Lisboa'),
    (POR, 'Porto'),
    (OUT, 'Outras Naturalidades')
)

default = ''
PRI = '1º Ciclo'
SEC = '2º Ciclo'
TER = '3º Ciclo'
LI = 'Licenciado'
DO = 'Doutorado'

HABILITACOES = (
    (default, '---'),
    (PRI, '1º Ciclo'),
    (SEC, '2º Ciclo'),
    (TER, '3º Ciclo'),
    (LI, 'Licenciado'),
    (DO, 'Doutorado')
)

default = ''
EE = 'Empregado Escritório'
TH = 'Técnico Hardware'
PRG = 'Programador'
PRO = 'Professor'
ADV = 'Advogado'

PROFISSOES = (
    (default, '---'),
    (EE, 'Empregado Escritório'),
    (TH, 'Técnico Hardware'),
    (PRG, 'Programador'),
    (PRO, 'Professor'),
    (ADV, 'Advogado')
)

default = ''
ECO = 'Empregado por conta de outrém'
ECP = 'Empregado por conta própria'
PL = 'Profissional Liberal'
DES = 'Desempregado'

SIT_PROFISSIONAL = (
    (default, '---'),
    (ECO, 'Empregado por conta de outrém'),
    (ECP, 'Empregado por conta própria'),
    (PL, 'Profissional Liberal'),
    (DES, 'Desempregado'),
)

default = ''
C = 'Casado'
S = 'Solteiro'
D = 'Divorciado'
V = 'Viuvo'

ESTADO_CIVIL = (
    (default, '---'),
    (C, 'Casado'),
    (S, 'Solteiro'),
    (D, 'Divorciado'),
    (V, 'Viuvo'),
)


class SubscriptionForm(ModelForm):
    # id = forms.CharField(label='Número do Sócio', required=True, widget=forms.TextInput(
    #     attrs={'placeholder': 'Nº', 'class': 'form-control', 'readonly':'readonly'}))
    member_number = forms.IntegerField(initial=n_member, label='Nº Sócio', required=False,
                                       widget=forms.TextInput(
                                           attrs={'placeholder': 'Nº', 'class': 'form-control',
                                                  'readonly': 'readonly'}))

    name = forms.CharField(label='Nome', required=True, widget=forms.TextInput(
        attrs={'placeholder': 'nome', 'class': 'form-control'}))

    document_type = forms.ChoiceField(DOC, label='Documento Idendificação', required=False,
                                      widget=forms.Select(attrs={'placeholder': '--',
                                                                 'class': 'form-control input-sm medio required_form'})
                                      )
    admission_date = forms.DateField(label='Data de emissão', required=False,
                                     widget=forms.DateInput(
                                         attrs={'placeholder': 'dd/mm/yyyy', 'class': 'form-control date mask-date',
                                                ' data-provide': 'datepicker',
                                                'data-date-format': 'dd/mm/yyyy'}))

    doc_number = forms.CharField(label='Número do Documento', required=False, widget=forms.TextInput(
        attrs={'placeholder': 'Nº', 'class': 'form-control'}))

    doc_emmission = forms.CharField(label='Local de Emissão', required=False, widget=forms.TextInput(
        attrs={'placeholder': 'local', 'class': 'form-control'}))

    situation = forms.ChoiceField(SITUACAO, label='Situação', required=False, initial=True,
                                  widget=forms.Select(attrs={'placeholder': '--',
                                                             'class': 'form-control input-sm medio required_form'})
                                  )

    last_quota = forms.DateField(label='Última quota paga', required=False,
                                 widget=forms.DateInput(
                                     attrs={'placeholder': 'dd/mm/yyyy', 'class': 'form-control date mask-date',
                                            ' data-provide': 'datepicker',
                                            'data-date-format': 'dd/mm/yyyy'}))

    # category = forms.CharField(label='Categoria', required=True, widget=forms.TextInput(
    #     attrs={'placeholder': 'categoria', 'class': 'form-control'}))

    category = forms.ChoiceField(CATEGORY, label='Categoria', required=False,
                                 widget=forms.Select(attrs={'placeholder': '--',
                                                            'class': 'form-control input-sm medio required_form'})
                                 )

    profile = forms.ChoiceField(PERFIL, label='Perfil', required=False,
                                widget=forms.Select(attrs={'placeholder': '--',
                                                           'class': 'form-control input-sm medio required_form'})
                                )

    sex = forms.ChoiceField(SEX, label='Sexo', required=False, initial=True,
                            widget=forms.Select(attrs={'placeholder': '--',
                                                       'class': 'form-control input-sm medio required_form'})
                            )

    birthdate = forms.DateField(label='Data de Nascimento', required=False,
                                widget=forms.DateInput(
                                    attrs={'placeholder': 'dd/mm/yyyy', 'class': 'form-control date mask-date',
                                           ' data-provide': 'datepicker',
                                           'data-date-format': 'dd/mm/yyyy'}))

    # nationality = forms.CharField(label='Nacionalidade', required=False, widget=forms.TextInput(
    #     attrs={'placeholder': '', 'class': 'form-control'}))
    nationality = forms.ChoiceField(NACIONALIDADE, label='Nacionalidade', required=False,
                                    widget=forms.Select(attrs={'placeholder': '--',
                                                               'class': 'form-control input-sm medio required_form'})
                                    )

    # naturalness = forms.CharField(label='Naturalidade', required=False, widget=forms.TextInput(
    #     attrs={'placeholder': '', 'class': 'form-control'}))
    naturalness = forms.ChoiceField(NATURALIDADE, label='Naturalidade', required=False,
                                    widget=forms.Select(attrs={'placeholder': '--',
                                                               'class': 'form-control input-sm medio required_form'})
                                    )

    # marital_status = forms.CharField(label='Estado Civil', required=False, widget=forms.TextInput(
    #     attrs={'placeholder': '', 'class': 'form-control'}))
    marital_status = forms.ChoiceField(ESTADO_CIVIL, label='Estado Civil', required=False,
                                       widget=forms.Select(attrs={'placeholder': '--',
                                                                  'class': 'form-control input-sm medio required_form'})
                                       )

    # qualifications = forms.CharField(label='Habilitações', required=False, widget=forms.TextInput(
    #     attrs={'placeholder': '', 'class': 'form-control'}))
    qualifications = forms.ChoiceField(HABILITACOES, label='Habilitações', required=False,
                                       widget=forms.Select(attrs={'placeholder': '--',
                                                                  'class': 'form-control input-sm medio required_form'})
                                       )

    # profession = forms.CharField(label='Profissão', required=False, widget=forms.TextInput(
    #     attrs={'placeholder': '', 'class': 'form-control'}))

    profession = forms.ChoiceField(PROFISSOES, label='Profissão', required=False,
                                   widget=forms.Select(attrs={'placeholder': '--',
                                                              'class': 'form-control input-sm medio required_form'})
                                   )

    # professional_situation = forms.CharField(label='Situação Profissional', required=False, widget=forms.TextInput(
    #     attrs={'placeholder': '', 'class': 'form-control'}))
    professional_situation = forms.ChoiceField(SIT_PROFISSIONAL, label='Situação Profissional', required=False,
                                               widget=forms.Select(attrs={'placeholder': '--',
                                                                          'class': 'form-control input-sm medio required_form'})
                                               )

    father = forms.CharField(label='Nome do Pai', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    mother = forms.CharField(label='Nome da Mãe', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    residence_address = forms.CharField(label='Morada', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    city = forms.CharField(label='Cidade', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    postcode = forms.CharField(label='Código Postal', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    country = forms.CharField(label='País', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    telephone_1 = forms.CharField(label='Telemóvel', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    telephone_2 = forms.CharField(label='Telefone', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    email = forms.CharField(label='Email', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    #Sem campos no Model
    NIF = forms.CharField(label='Número de Identificação Fiscal', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    SWIFT = forms.CharField(label='SWIFT', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    tipo_cobranca = forms.CharField(label='Tipo de Cobrança', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    cobrador = forms.CharField(label='Cobrador', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    prioridade = forms.CharField(label='Periodicidade', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    banco = forms.CharField(label='Banco', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    pais_banco = forms.CharField(label='País do Banco', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    NIB_IBAN = forms.CharField(label='NIB/IBAN', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    class Meta:
        model = MemberProfile
        fields = '__all__'


# Formulário para a edição do utilizador
class EditSubscriptionForm(ModelForm):
    member_number = forms.IntegerField(initial=n_member, label='Nº Sócio', required=False,
                                       widget=forms.TextInput(
                                           attrs={'placeholder': 'Nº', 'class': 'form-control',
                                                  'readonly': 'readonly'}))

    name = forms.CharField(label='Nome', required=True, widget=forms.TextInput(
        attrs={'placeholder': 'nome', 'class': 'form-control'}))

    document_type = forms.ChoiceField(DOC, label='Documento Idendificação', required=False,
                                      widget=forms.Select(attrs={'placeholder': '--',
                                                                 'class': 'form-control input-sm medio required_form'})
                                      )
    admission_date = forms.DateField(label='Data de emissão', required=False,
                                     widget=forms.DateInput(
                                         attrs={'placeholder': 'dd/mm/yyyy', 'class': 'form-control date mask-date',
                                                ' data-provide': 'datepicker',
                                                'data-date-format': 'dd/mm/yyyy'}))

    doc_number = forms.CharField(label='Número do Documento', required=False, widget=forms.TextInput(
        attrs={'placeholder': 'Nº', 'class': 'form-control'}))

    doc_emmission = forms.CharField(label='Local de Emissão', required=False, widget=forms.TextInput(
        attrs={'placeholder': 'local', 'class': 'form-control'}))

    situation = forms.ChoiceField(SITUACAO, label='Situação', required=False, initial=True,
                                  widget=forms.Select(attrs={'placeholder': '--',
                                                             'class': 'form-control input-sm medio required_form'})
                                  )

    last_quota = forms.DateField(label='Última quota paga', required=False,
                                 widget=forms.DateInput(
                                     attrs={'placeholder': 'dd/mm/yyyy', 'class': 'form-control date mask-date',
                                            ' data-provide': 'datepicker',
                                            'data-date-format': 'dd/mm/yyyy'}))

    # category = forms.CharField(label='Categoria', required=True, widget=forms.TextInput(
    #     attrs={'placeholder': 'categoria', 'class': 'form-control'}))

    category = forms.ChoiceField(CATEGORY, label='Categoria', required=False,
                                 widget=forms.Select(attrs={'placeholder': '--',
                                                            'class': 'form-control input-sm medio required_form'})
                                 )

    profile = forms.ChoiceField(PERFIL, label='Perfil', required=False,
                                widget=forms.Select(attrs={'placeholder': '--',
                                                           'class': 'form-control input-sm medio required_form'})
                                )

    sex = forms.ChoiceField(SEX, label='Sexo', required=False, initial=True,
                            widget=forms.Select(attrs={'placeholder': '--',
                                                       'class': 'form-control input-sm medio required_form'})
                            )

    birthdate = forms.DateField(label='Data de Nascimento', required=False,
                                widget=forms.DateInput(
                                    attrs={'placeholder': 'dd/mm/yyyy', 'class': 'form-control date mask-date',
                                           ' data-provide': 'datepicker',
                                           'data-date-format': 'dd/mm/yyyy'}))

    # nationality = forms.CharField(label='Nacionalidade', required=False, widget=forms.TextInput(
    #     attrs={'placeholder': '', 'class': 'form-control'}))
    nationality = forms.ChoiceField(NACIONALIDADE, label='Nacionalidade', required=False,
                                    widget=forms.Select(attrs={'placeholder': '--',
                                                               'class': 'form-control input-sm medio required_form'})
                                    )

    # naturalness = forms.CharField(label='Naturalidade', required=False, widget=forms.TextInput(
    #     attrs={'placeholder': '', 'class': 'form-control'}))
    naturalness = forms.ChoiceField(NATURALIDADE, label='Naturalidade', required=False,
                                    widget=forms.Select(attrs={'placeholder': '--',
                                                               'class': 'form-control input-sm medio required_form'})
                                    )

    # marital_status = forms.CharField(label='Estado Civil', required=False, widget=forms.TextInput(
    #     attrs={'placeholder': '', 'class': 'form-control'}))
    marital_status = forms.ChoiceField(ESTADO_CIVIL, label='Estado Civil', required=False,
                                       widget=forms.Select(attrs={'placeholder': '--',
                                                                  'class': 'form-control input-sm medio required_form'})
                                       )

    # qualifications = forms.CharField(label='Habilitações', required=False, widget=forms.TextInput(
    #     attrs={'placeholder': '', 'class': 'form-control'}))
    qualifications = forms.ChoiceField(HABILITACOES, label='Habilitações', required=False,
                                       widget=forms.Select(attrs={'placeholder': '--',
                                                                  'class': 'form-control input-sm medio required_form'})
                                       )

    # profession = forms.CharField(label='Profissão', required=False, widget=forms.TextInput(
    #     attrs={'placeholder': '', 'class': 'form-control'}))

    profession = forms.ChoiceField(PROFISSOES, label='Profissão', required=False,
                                   widget=forms.Select(attrs={'placeholder': '--',
                                                              'class': 'form-control input-sm medio required_form'})
                                   )

    # professional_situation = forms.CharField(label='Situação Profissional', required=False, widget=forms.TextInput(
    #     attrs={'placeholder': '', 'class': 'form-control'}))
    professional_situation = forms.ChoiceField(SIT_PROFISSIONAL, label='Situação Profissional', required=False,
                                               widget=forms.Select(attrs={'placeholder': '--',
                                                                          'class': 'form-control input-sm medio required_form'})
                                               )

    father = forms.CharField(label='Nome do Pai', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    mother = forms.CharField(label='Nome da Mãe', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    residence_address = forms.CharField(label='Morada', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    city = forms.CharField(label='Cidade', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    postcode = forms.CharField(label='Código Postal', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    country = forms.CharField(label='País', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    telephone_1 = forms.CharField(label='Telemóvel', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    telephone_2 = forms.CharField(label='Telefone', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    email = forms.CharField(label='Email', required=False, widget=forms.TextInput(
        attrs={'placeholder': '', 'class': 'form-control'}))

    class Meta:
        model = MemberProfile
        fields = '__all__'
